import React from 'react';

import Select from '../Select.js';

CurrencySelectField.propTypes = {
    instrument: React.PropTypes.string,
    value: React.PropTypes.string,
    onChange: React.PropTypes.func
}

function CurrencySelectField(props) {
    let {instrument, style, ...attributes} = props;
    let currencySelectList = instrument.split("/").map((currency)=> {
        return (<option value={currency} key={`currency-menu-${currency}`}>{currency}</option>)
    });
    return (
        <select className="form-control input-transparent" style={Object.assign({},{width: "100%"},style)} {...attributes}>
            {currencySelectList}
        </select>
    )
}


export default CurrencySelectField;