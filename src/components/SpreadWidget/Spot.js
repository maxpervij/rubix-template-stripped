import React from 'react';

let arrowStyle = {
    width: "",
    height: "",
    color: ""
}
let spotStyle = {
    position: 'relative',
}

export default React.createClass({
    propTypes: {
        rate: React.PropTypes.number,
        refresh: React.PropTypes.bool,
        decimal: React.PropTypes.number,
        onClick: React.PropTypes.func,
        upArrowStyle: React.PropTypes.object,
        downArrowStyle: React.PropTypes.object
    },
    getDefaultProps(){
        return{
            decimal: 5
        }

    },
    getInitialState(){
        return {
            rise: null
        }
    },
    componentWillReceiveProps(props){
        this.replaceState({
            rise: this.props.refresh ? null : props.rate > this.props.rate ? "up" : props.rate < this.props.rate ? "down" : null
        })
    },
    render() {
        let splitedValue = this.props.rate.toFixed(this.props.decimal).toString().match(/(.*)(\d\d)(\d)/),
            rise;
        switch (this.state.rise) {
            case 'up':
                rise = (
                    <div className="spread-widget-spot-rise-arrow-up" style={arrowStyle}/>
                );
                break;
            case 'down':
                rise = (
                    <div className="spread-widget-spot-rise-arrow-down" style={arrowStyle}/>
                );
                break;
            default :
                rise = null;
        }

        return (
            <div className={this.props.className} style={spotStyle} onClick={this.props.onClick}>
                <p>{splitedValue[1]}<span>{splitedValue[2]}</span>{splitedValue[3]}</p>
                {rise}
            </div>
        )
    }
});