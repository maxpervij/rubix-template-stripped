import React from 'react';

import ForwardTenorsSpreadsList from './ForwardTenorsSpreadsList.js'

export default React.createClass({
    propTypes: {
        tenors: React.PropTypes.object,
        onQuote: React.PropTypes.func,
        onTenorsToggle: React.PropTypes.func
    },
    getInitialState() {
        return {
            opened: false,
            date: null

        };
    },
    handleSpreadsListOpen(){
        this.setState({
            opened: !this.state.opened
        },this.props.onTenorsToggle)
    },
    handleDateChange(event, date){
        this.setState({
            date
        })
    },
    handleOpenDateDialog(){
        this.datepicker.openDialog();
    },
    render() {
        if (this.state.opened) {
            return (
                <div className="spread-widget-forward-tenors">
                    <div className="spread-widget-tenors-header align-center" onClick={this.handleSpreadsListOpen}>
                        FORWARD TENORS
                        <div
                            style={{position: "absolute",top: "0",left: "10px",color:"#2e507a",width:"20px",height:"20px"}}/>
                    </div>

                    <div className="spread-widget-tenors-container">
                        <ForwardTenorsSpreadsList tenors={this.props.tenors} onQuote={this.props.onQuote}/>

                        <div className="spread-widget-tenors-footer container-fluid">
                            <div className="row">
                                <div className="col-xs-7">


                                </div>
                                <div className="col-xs-5">
                                    <button label="Price" backgroundColor="#404040" disabled={!this.state.date}
                                                  onTouchTap={()=>{this.props.onQuote({date:this.state.date.toLocaleDateString()})}}
                                                  style={{height:"25px",lineHeight: "25px", overflow: "hidden", margin: "10px 0",minWidth:0, width: "100%",borderRadius: "5px"}}
                                                  labelStyle={{textTransform: "none", color:"#ffffff", fontWeight: "400",fontSize: "13px"}}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="spread-widget-forward-tenors">
                    <header className="spread-widget-tenors-header align-center" onClick={this.handleSpreadsListOpen}>
                        FORWARD TENORS
                        <div
                            style={{position: "absolute",top: "0",left: "10px",color:"#2e507a",width:"20px",height:"20px"}}/>
                    </header>
                </div>
            )
        }
    }
});


/*
*<div mode="landscape" textFieldStyle={{width: "100%"}} hintText="Odd date"
 ref={(datepicker) => {this.datepicker = datepicker}}
 onChange={this.handleDateChange}/>
 <div color="#ccc"
 style={{position:"absolute", right:"15px", top:"10px", cursor: "pointer"}}
 onClick={this.handleOpenDateDialog}
 />
* */