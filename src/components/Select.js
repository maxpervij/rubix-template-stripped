/**
 * Created by Max Kudla.
 */

'use strict';

import React from 'react';

export default function (props) {
    let {options, value, style, ...attributes} = props;

    let list = options.map((option)=>{
        return(
            <option value={option} selected={option == value} key={`select-option-${option}`}
                    style={Object.assign({},{width: "100%"},style)} {...attributes}>{option}</option>
        )
    });
    return(
        <select className="form-control" >
            {list}
        </select>
    )
}
