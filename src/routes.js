import React from 'react';
import classNames from 'classnames';
import { IndexRoute, Route } from 'react-router';

import { Grid, Row, Col, MainContainer } from '@sketchpixy/rubix';

/* Common Components */

import Sidebar from './common/sidebar';
import Header from './common/header';

/* Pages */

import Default from './routes/Default';

import Panels from './demo/Panels';

import LineSeries from './demo/LineSeries';
import AreaSeries from './demo/AreaSeries';
import BarColSeries from './demo/BarColSeries';
import MixedSeries from './demo/MixedSeries';
import PieDonutSeries from './demo/PieDonutSeries';

import Chartjs from './demo/Chartjs';
import C3js from './demo/C3js';
import Morrisjs from './demo/Morrisjs';

import StaticTimeline from './demo/StaticTimeline';
import InteractiveTimeline from './demo/InteractiveTimeline';

import Codemirrorjs from './demo/Codemirrorjs';
import Maps from './demo/Maps';
import Editor from './demo/Editor';

import Buttons from './demo/Buttons';
import Dropdowns from './demo/Dropdowns';
import TabsAndNavs from './demo/TabsAndNavs';
import Sliders from './demo/Sliders';
import Knobs from './demo/Knobs';
import Modals from './demo/Modals';
import Messengerjs from './demo/Messengerjs';

import Controls from './demo/Controls';
import XEditable from './demo/XEditable';
import Wizard from './demo/Wizard';

import Tables from './demo/Tables';
import Datatablesjs from './demo/Datatablesjs';
import Tablesawjs from './demo/Tablesawjs';

import Grids from './demo/Grids';
import Calendar from './demo/Calendar';

import Dropzonejs from './demo/Dropzonejs';
import Cropjs from './demo/Cropjs';

import Fonts from './demo/Fonts';

import Login from './demo/Login';
import Signup from './demo/Signup';
import Invoice from './demo/Invoice';
import Pricing from './demo/Pricing';

import Lock from './demo/Lock';



class App extends React.Component {
  render() {
    return (
      <MainContainer {...this.props}>
        <Sidebar />
        <Header />
        <div id='body'>
          {this.props.children}
        </div>
      </MainContainer>
    );
  }
}

const routes = (
  <Route path='/' component={App}>
    <IndexRoute component={Default} />

    <Route path='submenu' component={Default} />

    <Route path='panels' component={Panels} />
    <Route path='charts/rubix/line' component={LineSeries} />
    <Route path='charts/rubix/area' component={AreaSeries} />
    <Route path='charts/rubix/barcol' component={BarColSeries} />
    <Route path='charts/rubix/mixed' component={MixedSeries} />
    <Route path='charts/rubix/piedonut' component={PieDonutSeries} />
    <Route path='charts/chartjs' component={Chartjs} />
    <Route path='charts/c3js' component={C3js} />
    <Route path='charts/morrisjs' component={Morrisjs} />
    <Route path='timeline' component={StaticTimeline} />
    <Route path='interactive-timeline' component={InteractiveTimeline} />
    <Route path='codemirror' component={Codemirrorjs} />
    <Route path='maps' component={Maps} />
    <Route path='editor' component={Editor} />
    <Route path='ui-elements/buttons' component={Buttons} />
    <Route path='ui-elements/dropdowns' component={Dropdowns} />
    <Route path='ui-elements/tabs-and-navs' component={TabsAndNavs} />
    <Route path='ui-elements/sliders' component={Sliders} />
    <Route path='ui-elements/knobs' component={Knobs} />
    <Route path='ui-elements/modals' component={Modals} />
    <Route path='ui-elements/messenger' component={Messengerjs} />
    <Route path='forms/controls' component={Controls} />
    <Route path='forms/x-editable' component={XEditable} />
    <Route path='forms/wizard' component={Wizard} />
    <Route path='tables/bootstrap-tables' component={Tables} />
    <Route path='tables/datatables' component={Datatablesjs} />
    <Route path='tables/tablesaw' component={Tablesawjs} />
    <Route path='grid' component={Grids} />
    <Route path='calendar' component={Calendar} />
    <Route path='file-utilities/dropzone' component={Dropzonejs} />
    <Route path='file-utilities/crop' component={Cropjs} />
    <Route path='fonts' component={Fonts} />
    <Route path='invoice' component={Invoice} />
    <Route path='pricing' component={Pricing} />

    <Route path="*" component={Default}/>
  </Route>
);

export default routes;
