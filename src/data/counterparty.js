let counterparty = [
    {
        name: "Company A",
        tier: "T1"
    },
    {
        name: "Company B",
        tier: "T2"
    },
    {
        name: "Company C",
        tier: "T3"
    }
]

export default  counterparty;