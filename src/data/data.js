let data = {
    "T1":[
        {
            date: "12/05/2016",
            bid: 1.11411,
            ask: 1.11452,
            instrument: "EUR/USD",
            tenors: {
                "1W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "2W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "3W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "4W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "1M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "2M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "3M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                }
            }
        },
        {
            date: "12/05/2016",
            bid: 1.11411,
            ask: 1.11452,
            instrument: "USD/EUR",
            tenors: {
                "1W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "2W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "3W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "4W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "1M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "2M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "3M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                }
            }
        },
        {
            date: "12/05/2016",
            bid: 1.11411,
            ask: 1.11452,
            instrument: "EUR/JPY",
            tenors: {
                "1W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "2W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "3W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "4W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "1M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "2M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "3M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                }
            }
        },
        {
            date: "12/05/2016",
            bid: 1.11411,
            ask: 1.11452,
            instrument: "JPY/EUR",
            tenors: {
                "1W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "2W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "3W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "4W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "1M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "2M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                },
                "3M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 119.937,
                    ask: 119.957
                }
            }
        },
    ],
    "T2":[
        {
            date: "12/05/2016",
            bid: 2.11411,
            ask: 2.11452,
            instrument: "EUR/USD",
            tenors: {
                "1W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "2W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "3W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "4W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "1M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "2M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "3M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                }
            }
        },
        {
            date: "12/05/2016",
            bid: 2.11411,
            ask: 2.11452,
            instrument: "USD/EUR",
            tenors: {
                "1W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "2W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "3W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "4W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "1M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "2M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "3M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                }
            }
        },
        {
            date: "12/05/2016",
            bid: 2.11411,
            ask: 2.11452,
            instrument: "EUR/JPY",
            tenors: {
                "1W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "2W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "3W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "4W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "1M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "2M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "3M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                }
            }
        },
        {
            date: "12/05/2016",
            bid: 2.11411,
            ask: 2.11452,
            instrument: "JPY/EUR",
            tenors: {
                "1W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "2W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "3W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "4W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "1M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "2M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                },
                "3M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 120.937,
                    ask: 120.957
                }
            }
        },
    ],
    "T3":[
        {
            date: "12/05/2016",
            bid: 3.11411,
            ask: 3.11452,
            instrument: "EUR/USD",
            tenors: {
                "1W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "2W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "3W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "4W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "1M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "2M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "3M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                }
            }
        },
        {
            date: "12/05/2016",
            bid: 3.11411,
            ask: 3.11452,
            instrument: "USD/EUR",
            tenors: {
                "1W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "2W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "3W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "4W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "1M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "2M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "3M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                }
            }
        },
        {
            date: "12/05/2016",
            bid: 3.11411,
            ask: 3.11452,
            instrument: "EUR/JPY",
            tenors: {
                "1W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "2W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "3W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "4W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "1M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "2M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "3M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                }
            }
        },
        {
            date: "12/05/2016",
            bid: 3.11411,
            ask: 3.11452,
            instrument: "JPY/EUR",
            tenors: {
                "1W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "2W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "3W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "4W": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "1M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "2M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                },
                "3M": {
                    date: "01/01/2015",
                    title: "T1",
                    bid: 130.937,
                    ask: 130.957
                }
            }
        }
    ]
}

export default data;